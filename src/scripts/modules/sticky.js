// import $ from 'jquery';
import sticky from 'jquery-sticky';

export default () => {
    ((sticky, $) => {
        'use strict';

        const $dom = {};

        const cacheDom = () => {
            $dom.stickyElement = $(".site-header");
        };

        const stickyEle = () => {
            $dom.stickyElement.sticky({ topSpacing: 0 });
        };

        sticky.init = () => {
            cacheDom();
            stickyEle();
        };

    })(window.sticky = window.sticky || {}, $);
}
