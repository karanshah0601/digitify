// import $ from 'jquery';
import slick from 'slick-carousel';
import Instafeed from "../../assets/instafeed";
import typewriter from '../../assets/aeo-typewriter';
export default () => {
    ((custom, $) => {
        const $dom = {};

        const cacheDom = () => {
            $dom.bodyElement = $('body');
        };
        const bindUIActions = () => {
            $(".toggle-btn").click(function() {
                $(".mobile-menu").css({
                    left: "0%"
                });
            });
            $(".close-btn").click(function() {
                $(".mobile-menu").css({
                    left: "-100%"
                });
            });
            //form login
            $("#forget").click(function() {
                $(".lgn").hide();
                $("#recover").show();
            });
            $(".cnl").click(function() {
                $("#recover").hide();
                $(".lgn").show();
            });
            //PDP Qty Selector
            $("button.qty-btns").on("click", function(event) {
                event.preventDefault();
                var container = $(event.currentTarget).closest('[data-qtyContainer]');
                var qtEle = $(container).find('[data-qty]');
                var currentQty = $(qtEle).val();
                var qtyDirection = $(this).data("direction");
                var newQty = 0;

                if (qtyDirection == "1") {
                    newQty = parseInt(currentQty) + 1;
                } else if (qtyDirection == "-1") {
                    newQty = parseInt(currentQty) - 1;
                }

                if (newQty == 1) {
                    $(".decrement-quantity").attr("disabled", "disabled");
                }
                if (newQty > 1) {
                    $(".decrement-quantity").removeAttr("disabled");
                }

                if (newQty > 0) {
                    newQty = newQty.toString();
                    $(qtEle).val(newQty);
                } else {
                    $(qtEle).val("1");
                }
            });
            //   var selectCallback = function(variant, selector){
            //     if (variant) {
            //     var form = jQuery('#' + selector.domIdPrefix).closest('form');
            //     for (var i=0,length=variant.options.length; i<length; i++) {
            //       var radioButton = form.find('.swatch[data-option-index="' + i + '"] :radio[value="' + variant.options[i] +'"]');
            //       if (radioButton.size()) {
            //         radioButton.get(0).checked = true;
            //       }
            //     }
            //   }
            // };
            // jQuery(function() {
            //     jQuery('.swatch :radio').change(function() {
            //         var optionIndex = jQuery(this).closest('.swatch').attr('data-option-index');
            //         var optionValue = jQuery(this).val();
            //         jQuery(this)
            //             .closest('form')
            //             .find('.single-option-selector')
            //             .eq(optionIndex)
            //             .val(optionValue)
            //             .trigger('change');
            //     });
            // });
            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip(); 
                var wow = new WOW(
                    {
                      boxClass:     'wow',      // animated element css class (default is wow)
                      animateClass: 'animated', // animation css class (default is animated)
                      offset:       0,          // distance to the element when triggering the animation (default is 0)
                      mobile:       true,       // trigger animations on mobile devices (default is true)
                      live:         true,       // act on asynchronously loaded content (default is true)
                      callback:     function(box) {
                        // the callback is fired every time an animation is started
                        // the argument that is passed in is the DOM node being animated
                      },
                      scrollContainer: null // optional scroll container selector, otherwise use window
                    }
                );
                wow.init();                   
              
                $('.textwriter-text').each(function(){
                    var text_data=$(this).html();
                    $(this).empty();                    
                    $(this).typewriter({
                        text: text_data,
                        waitingTime: 1000,
                        delay: 300,
                        hide: 0,
                        cursor: false,
                    });
                });
                    // var text_data=$('.textwriter-text').html();
                    // $('.textwriter-text').empty();
                    // console.log(text_data);
                    // $('.textwriter-text').typewriter({
                    //     text: text_data,
                    //     waitingTime: 1000,
                    //     delay: 100,
                    //     hide: 1,
                    //     cursor: true,
                    // });
                    $('.main_related').slick({
                        dots: false,
                        arrows:false,
                        infinite: true,
                        rtl:true, 
                        autoplay: true,
                        autoplaySpeed: 2000,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        responsive: [
                          {
                            breakpoint: 1024,
                            settings: {
                              slidesToShow: 4,
                              slidesToScroll: 1,
                              infinite: true
                            }
                          },
                           {
                            breakpoint: 992,
                            settings: {
                              slidesToShow: 3,
                              slidesToScroll: 1
                            }
                          },
                          {
                            breakpoint: 767,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1
                            }
                          },
                          {
                            breakpoint: 480,
                            settings: {
                              slidesToShow:1,
                              slidesToScroll: 1
                            }
                          }
                          // You can unslick at a given breakpoint now by adding:
                          // settings: "unslick"
                          // instead of a settings object
                        ]
                    });
                $('.main-slider-book').slick({
                    dots: false,
                    arrows:true,
                    infinite: false,
                    rtl:true, 
                    autoplay: true,
                    autoplaySpeed: 2000,
                    slidesToShow: 1,
                    slidesToScroll: 1
                });
                $('.conver-item-ul').slick({
                    dots: false,
                    arrows:false,
                    infinite: true,
                    rtl:true, 
                    autoplay: true,
                    autoplaySpeed: 2000,
                    slidesToShow: 6,
                    slidesToScroll: 1,
                    responsive: [
                      {
                        breakpoint: 1024,
                        settings: {
                          slidesToShow: 6,
                          slidesToScroll: 1,
                          infinite: true
                        }
                      },
                       {
                        breakpoint: 992,
                        settings: {
                          slidesToShow: 4,
                          slidesToScroll: 1
                        }
                      },
                      {
                        breakpoint: 767,
                        settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1
                        }
                      },
                      {
                        breakpoint: 480,
                        settings: {
                          slidesToShow:2,
                          slidesToScroll: 1
                        }
                      }
                      // You can unslick at a given breakpoint now by adding:
                      // settings: "unslick"
                      // instead of a settings object
                    ]
                });
                $("#quickby").click(function() {
                    addtocart = $('#AddToCartForm');
                    jQuery.ajax({
                        type: 'POST',
                        url: '/cart/add.js',
                        data: jQuery(addtocart).serialize(),
                        dataType: 'json',
                        success: function(cart) {
                            $.ajax({
                                url: '/cart',
                                success: function success(data) {
                                    window.location.href = "/checkout";
                                }
                            });
                        }
                    });
                });
                var User_Id = $("#instafeed").data("user_id");
                var Access_Token = $("#instafeed").data("access_token");
                var limit = $("#instafeed").data("limit");
                if (User_Id) {
                    var userFeed = new Instafeed({
                        get: "user",
                        userId: User_Id,
                        accessToken: Access_Token,
                        limit: limit,
                        resolution: "standard_resolution",
                        template: '<div class="col-lg-2 col-md-4 mb-3 col-6"><div class="video-box"><img  class="img-responsive"  src="{{image}}"></div></div>'
                    });
                    userFeed.run();
                }
                // $(".audio").click(function() {
                //     var d = $(this).data("id");
                //     var x = document.getElementById(d);
                //     x.pause();
                // });
                $('body').click(function(){
                    if(!$( ".modal" ).hasClass( "show" )){
                        $('audio').each(function(){
                            this.pause(); // Stop playing
                            this.currentTime = 0; // Reset time
                        }); 
                        $('video').each(function(){
                            this.pause(); // Stop playing
                            this.currentTime = 0; // Reset time
                        }); 
                    }
                });
                // var limit_words=100;
                $(".qty-new").keyup(function(d) {
                    var v = $(this).data("vid");
                    var p = $(this).data('price');
                    // var $option = $(this).find('option:selected');
                    // var value = $option.val();
                    var value = $(this).val();
                    var price = parseFloat(p / 100).toFixed(2);
                    var rs = parseFloat(price * value).toFixed(2);
                    //document.getElementById('price-' + v).value = rs;
                    $('.price-' + v).val(rs);
                //NEW     
                    var nid=$(this).data("nid");
                    var prc= parseFloat($(".o-price-"+nid).text()).toFixed(2);
                    var rs1= parseFloat(prc*value).toFixed(2);
                    $('.price-' + nid).val(rs1);
                    //new mobile
                    var nid=$(this).data("nid");
                    var prc= parseFloat($(".o-price-mobile-"+nid).text()).toFixed(2);
                    var rs1= parseFloat(prc*value).toFixed(2);
                    $('.price-mobile-' + nid).val(rs1);
                    // limit_words=value;
                    // $("#total_counts").text(limit_words);
                });
                $('.source').on('change',function(){
                    var languages=[ "אוקראינית" , "איטלקית" , "אינדונזית" , "אנגלית" , "בולגרית" , "גרמנית" , "דנית" , "הונגרית" , "הינדי" , "וייטנאמית" , "טאגאלוג" , "טורקית" , "יוונית" , "יידיש" , "יפנית" , "לטינית" , "מלאית" , "נורבגית" , "ניגרית" , "סינית מפושטת" , "סלובקית" , "ספרדית" , "סרבית" , "עברית" , "ערבית" , "פולנית" , "פורטוגזית" , "פינית" , "צ'כית" , "צרפתית" , "קוריאנית" , "קטלאנית" , "רומנית" , "רוסית" , "שוודית" , "תאית" ];            
                    var source= $(this).val();
                    var id= $(this).data("id");                     
                    var i=$(this).attr('id');          
                    $('#target'+id).empty();    
                    $.each(languages, function( index, data ) {
                        if(source != data){
                            $('#target'+id).append($('<option>').val(data).text(data));
                        }
                    });
                    var target=$('#target'+id).val(); 
                    
                    if(i == 'source-mobile-'+id){
                        $('#target-mobile-'+id).empty();  
                        $.each(languages, function( index, data ) {
                            if(source != data){
                            $('#target-mobile-'+id).append($('<option>').val(data).text(data));
                        }
                        });
                       var target=$('#target-mobile-'+id).val();
                    }

                    // if( (source == "אנגלית") || (source == "עברית" && target == "רוסית") || (source == "עברית" && target == "אנגלית") || (source == "רוסית" && target == "עברית") || (source == "רוסית" && target == "אנגלית"))    
                    if( source == "עברית" || source == "רוסית"  || source == "אנגלית" || target == "עברית" || target == "רוסית"  || target == "אנגלית" )
                    {     
                        jQuery(this).closest('form').find('.single-option-selector').eq(0).val('סוג 1').trigger('change');

                    }else{
                      
                        jQuery(this).closest('form').find('.single-option-selector').eq(0).val('סוג 2').trigger('change');
                    }
                    // jQuery(this).closest('form').find('.single-option-selector').eq(0).val('english').trigger('change');
                    // jQuery(this).closest('form').find('.single-option-selector').eq(0).val('hebrew').trigger('change');
                 });
                 $('.target').on('change',function(){
                    var target=$(this).val();
                    var id= $(this).data("id");
                    var i=$(this).attr('id');
                    var source=$('#source'+id).val();
                    if(i == 'target-mobile-'+id){
                        var source=$('#source-mobile-'+id).val();
                    }
                    // if( (source == "אנגלית") || (source == "עברית" && target == "רוסית") || (source == "עברית" && target == "אנגלית") || (source == "רוסית" && target == "עברית") || (source == "רוסית" && target == "אנגלית"))    
                    if( source == "עברית" || source == "רוסית"  || source == "אנגלית" || target == "עברית" || target == "רוסית"  || target == "אנגלית" )
                    {     
                        jQuery(this).closest('form').find('.single-option-selector').eq(0).val('סוג 1').trigger('change');
                    }else{
                        jQuery(this).closest('form').find('.single-option-selector').eq(0).val('סוג 2').trigger('change');
                    }
                    // jQuery(this).closest('form').find('.single-option-selector').eq(0).val('english').trigger('change');
                    // jQuery(this).closest('form').find('.single-option-selector').eq(0).val('hebrew').trigger('change');

                 });
                 $('#content').on('keyup',function(){
                    var con="";
                    var words=0;
                    $('#w_count').text("");
                    if($("#content").val().length != 0){
                    words = $.trim($("#content").val()).length ? this.value.match(/\S+/g).length : 0;
                    //     if (words > limit_words) {
                    //     var trimmed = $("#content").val().split(/\s+/, limit_words).join(" ");
                    //     con=$(this).val(trimmed + " ");
                    //     }   
                    //     else{
                    //         con=$(this).val();
                    //     }
                    console.log(words);
                    }
                    $('#w_count').text(words);
 
                    $('.content').each(function(){
                        $(this).val(con);
                    });
                });
                $(".files").on('change',function(){
                    if($(this).val() != "" ){
                        var file = this.files[0];
                        var fileType=file["type"];
                        var id=$(this).data("id");
                        var validImageTypes = [ "image/jpg","image/jpeg", "image/png","application/pdf"];
                        var vTypes=[".jpg",".jpeg",".png",".png"];
                        if ($.inArray(fileType, validImageTypes) < 0) {
                            // invalid file type code goes here.
                            $("#error-"+id).removeClass("d-none");
                            $("#error-mobile"+id).removeClass("d-none");
                            $("#error-"+id).addClass("d-block");
                            $("#error-mobile"+id).addClass("d-block");
                            $("#error-"+id).html("הקובץ אינו חוקי, הרחבות מותרות הן:<br>" + vTypes.join(", "));
                            $("#error-mobile-"+id).html("הקובץ אינו חוקי, הרחבות מותרות הן:<br>" + vTypes.join(", "));
                        }else{
                            var filename = $(this).val().replace(/.*(\/|\\)/, '');
                            if($("#error-"+id).hasClass("d-block")||$("#error-mobile-"+id).hasClass("d-block"))
                            {                           
                                $("#error-"+id).removeClass("d-block");
                                $("#error-mobile"+id).removeClass("d-block"); 
                                $("#error-"+id).addClass("d-none");
                                $("#error-mobile"+id).addClass("d-none");
                            }                    
                        $("#upload-btn-"+id).html(filename.substring(0,8));
                        $("#upload-btn-mobile-"+id).html(filename.substring(0,8));
                        }

                    }
                    
                });
                // Cart update

                // function updateCart(){
                //     setTimeout(() => {
                //     $("#checkoutCart").on('click', function (event) {
                //     // event.preventDefault();
                //         Shopify.queue = [];
                //         $('[data-cartitem]').each(function(index, item){
                //         var itemData = {};
                //         let properties = {};
                //         // var removePropID = $('[data-productRemoveID]').val();
                //         let propInput = $(item).find('[data-propName]');
                //         $(propInput).each(function(){
                //         let name = this.name.replace('properties[', '').replace(']', '');
                        
                //         if($.trim(this.value).length > 0){     
                //         properties[name] = this.value;
                //         console.log(name,'===>',properties[name]);
                          
                //     }
                //         })
                //         // properties['Delivery-Date'] = $(item).attr('data-date');
                //         // properties['Remove-id'] = $(item).attr('data-rem');
                //         itemData['index'] = $(item).data('id');
                //         itemData['qty'] = $(item).data('qty');
                //         itemData['properties'] = properties;
                        
                //         Shopify.queue.push(itemData);
                //         })
                //         Shopify.moveAlong = function() {
                //         if (Shopify.queue.length) {
                //         var request = Shopify.queue.shift();
                //         Shopify.updateItem(request.index, request.qty, request.properties, Shopify.moveAlong);
                //         }
                //         // else {
                //         // document.location.href = '/cart';
                //         // }
                //         };
                //         Shopify.updateItem = function(index, qty, properties, callback) {
                //             var params = {
                //             quantity: qty,
                //             line: index,
                //             properties: properties
                //     };
                //     console.log("properties==>", properties);
                //     // if(properties != false){
                //     // params.properties = properties;
                //     // }
                //     $.ajax({
                //     type: 'POST',
                //     url: '/cart/change.js',
                //     dataType: 'json',
                //     data: params,
                //     success: function(){
                //     if(typeof callback === 'function'){
                //     callback();
                //     }
                //     location.reload();
                //     },
                //     error: function(){}
                //     });
                //     }
                    
                //         Shopify.moveAlong();
                //         // else {
                //         // event.preventDefault();
                //         // $('[data-cart-bar]').removeClass('d-none');
                //         // }
                    
                //     })
                //     }, 1000);
                    
                // }
                // Cart Update
                // updateCart();
                // $(".add-cart").click(function() {
                //     var addcart = $(this).data("product-id");
                //     console.log(addcart);
                //     addtocart('#AddToCartForm-' + addcart);
                // });

                // function addtocart(addtocart_data) {
                //     console.log(addtocart_data);
                    // jQuery.ajax({
                    //     type: "POST",
                    //     url: "/cart/add.js",
                    //     data: jQuery(addtocart_data).serialize(),
                    //     dataType: "json",
                    //     success: function(cart) {}
                    // });
                // }
            });

            // $(document).ready(function () {
            //   $('#AddToCart').on('click', function (event) {
            //     event.preventDefault();
            //     const $form = $(this).closest('form');
            //     Shopify.queue = [];
            //     const variantID = $form.find('[name="id"]').val();
            //     let Qty = parseInt($('[name="quantity"]').val());
            //     Shopify.queue.push({
            //       variantId: variantID,
            //       quantity: Qty
            //     });
            //     $('[data-queue]').each(function () {
            //       if ($(this).find('input[type="checkbox"]').prop('checked') == true) {
            //         let variantID = $(this).find('input[type="checkbox"]').val();
            //         Qty = 1;
            //         let properties = {};
            //         properties["Addons"] = true;

            //         Shopify.queue.push({
            //           variantId: variantID,
            //           quantity: Qty,
            //           properties: properties
            //         });
            //       }
            //     })
            //     console.log(Shopify.queue)
            //     Shopify.moveAlong();
            //   })

            //   Shopify.moveAlong = function () {
            //     if (Shopify.queue.length) {
            //       var request = Shopify.queue.shift();
            //       Shopify.addItem(request.variantId, request.quantity, request.properties, Shopify.moveAlong);
            //     } else {
            //       //Shopify.AjaxifyCart.init();
            //       document.location.href = '/cart';
            //     }
            //   };

            //   Shopify.addItem = function (id, qty, properties, callback) {
            //     var params = {
            //       quantity: qty,
            //       id: id
            //     };
            //     if (properties != false) {
            //       params.properties = properties;
            //     }
            //     $.ajax({
            //       type: 'POST',
            //       url: '/cart/add.js',
            //       dataType: 'json',
            //       data: params,
            //       success: function () {
            //         if (typeof callback === 'function') {
            //           callback();
            //         }
            //       },
            //       error: function () {}
            //     });
            //   }
            // });


        };
        custom.init = () => {
            cacheDom();
            bindUIActions();
        };

    })(window.custom = window.custom || {}, $);
}